import java.util.jar._

import sbt.Keys.publish

organization in ThisBuild := "com.gu"

scalaVersion in ThisBuild := "2.13.4"

publishArtifact := false

publishTo := Some("uxforms-public-parent" at "s3://artifacts-public.uxforms.net")

publishArtifact := false

scalacOptions in ThisBuild += "-deprecation"

lazy val guardianResolver = resolvers += "Guardian Github" at "https://guardian.github.com/maven/repo-releases"

libraryDependencies += guice


def managementProject(name: String) = Project(name, file(name)).settings(Seq(
  javacOptions := Seq(
    "-g",
    "-encoding", "utf8"
  ),
  scalacOptions := Seq("-unchecked", "-deprecation",
    "-Xcheckinit", "-encoding", "utf8", "-feature",
    "-Xfatal-warnings"
  )
):_*)



lazy val root = Project("management-root", file(".")).enablePlugins(PlayScala).aggregate(
  managementPlay,
  examplePlay)
  .dependsOn(managementPlay,examplePlay)
  .settings(
    publish := {},
    publishLocal := {}
  )

lazy val managementPlay = managementProject("management-play")
  .settings(guardianResolver)
  .settings(libraryDependencies ++= Seq(
    "com.google.code.findbugs" % "jsr305" % "1.3.9"
  )
)

lazy val examplePlay = Project(
  "example",
  file("example"))
  .enablePlugins(PlayScala)
  .dependsOn(managementPlay)
  .settings(
    guardianResolver,
    publish := {},
    publishLocal := {}
  )

Compile / run := examplePlay / Compile / run
