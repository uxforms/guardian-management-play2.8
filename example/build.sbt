
publishArtifact := false

val guardianManagementVersion = "5.49"

//TODO fix multiproject build
libraryDependencies ++= Seq(
  "com.gu" %% "management" % guardianManagementVersion,
  "com.gu" %% "management-internal" % guardianManagementVersion,
  "com.gu" %% "management-logback" % guardianManagementVersion,
  "com.typesafe.play" %% "play" % "2.8.1",
  "com.typesafe.play" %% "play-test" % "2.8.1" % "test",
  "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % "test",
  "com.typesafe.play" %% "play-guice" % "2.8.1"
)

// needed for Play
resolvers += Resolver.typesafeRepo("releases")
