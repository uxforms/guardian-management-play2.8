name := "management-play28"

resolvers += "uxforms-public" at "https://artifacts-public.uxforms.net"

publishTo := Some("uxforms-public-management" at "s3://artifacts-public.uxforms.net")

packageBin / packageOptions +=
  Package.ManifestAttributes(
      java.util.jar.Attributes.Name.IMPLEMENTATION_VERSION -> version.value,
      java.util.jar.Attributes.Name.IMPLEMENTATION_TITLE -> name.value,
      java.util.jar.Attributes.Name.IMPLEMENTATION_VENDOR -> "guardian.co.uk"
  )

val guardianManagementVersion = "5.49"

libraryDependencies ++= Seq(
    "com.gu" %% "management" % guardianManagementVersion,
    "com.gu" %% "management-internal" % guardianManagementVersion,
    "com.gu" %% "management-logback" % guardianManagementVersion,
    "com.typesafe.play" %% "play" % "2.8.1",
    "com.typesafe.play" %% "play-test" % "2.8.1" % "test",
    "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % "test",
    "com.typesafe.play" %% "play-guice" % "2.8.1"
)

// needed for Play
resolvers += Resolver.typesafeRepo("releases")

// disable publishing the main javadoc jar
publishArtifact in (Compile, packageDoc) := false
