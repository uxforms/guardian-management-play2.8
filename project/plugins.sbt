resolvers ++= Seq(
  Classpaths.typesafeReleases
)

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.1")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.13")

addSbtPlugin("com.frugalmechanic" % "fm-sbt-s3-resolver" % "0.18.0")
